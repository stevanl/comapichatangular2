(function (angular) {
    'use strict';

    angular.module('compapiChat')
        .constant("appConfig", {
            apiSpaceId: "e0f8f569-6e87-427e-8fba-472b42a192c2",
            messagePageSize: 50
        });

})(window.angular);
